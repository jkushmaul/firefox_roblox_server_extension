
var CONST = {
    PAGE_SIZE: 10
}


function log(msg) {
    console.log("roblox extension: " + msg);
}
function getPlaceId(path) {
    var matches = path.match(/\/games\/([\d]+)\//i);
    return matches[1];
}

function getTimestampMs() {
    return ( + new Date());
}

function createUrl(idx) {
    var start_index =  idx * CONST.PAGE_SIZE;
    return "https://www.roblox.com/games/getgameinstancesjson?placeId=" + CONFIG.place_id + "&startIndex=" + start_index + "&_=" +  getTimestampMs();
}

function getServersByFetch(index) {
    var url =createUrl(index);
    log("Fetching " + url);
    return fetch(url, {
            headers: {'Content-Type': 'application/json'},
            credentials: "same-origin",
            mode: "same-origin"
        })
            .then(response => {            
                if (!response.ok) {
                    throw Error("Failure");
                }
                return response.json();
            })
            .then(data => {
                var servers = transformServerCollection(data.Collection);
                return servers;
            });
}

function transformServerCollection(serverCollection) {
    var result = [];
    serverCollection.forEach(server => {
        var xfrm = {
            guid: server.Guid,
            ping: server.Ping,
            fps: server.Fps,
            players: {
                max: server.Capacity,
                current: server.CurrentPlayers.length
            }
        };
        result.push(xfrm);
    });
    return result;
}

function getServerListCache() {
    var server_list_cache = null;
    var server_list_cache_str = sessionStorage.getItem(CONFIG.place_id + "_serverList");
    if (server_list_cache_str) {
        server_list_cache = JSON.parse(server_list_cache_str);
    }
    return server_list_cache;
}
function setServerlistCache(server_list_cache) {
    sessionStorage.setItem(CONFIG.place_id + "_serverList", JSON.stringify(server_list_cache));
}
function getLastServerPlayed() {
    return sessionStorage.getItem(CONFIG.place_id + "_lastServerJoined");
}
function setLastServerPlayed(server_guid) {
    log("Setting last server played: " + server_guid);
    return sessionStorage.setItem(CONFIG.place_id + "_lastServerJoined", server_guid);
}

function refreshServerLists() {
    var promises = [];
    log("refreshServerList(): Fetching server lists");
    for(var i = 0; i < CONFIG.max_pages; i++) {
        promises.push(getServersByFetch(i));
    }
    Promise.all(promises).then( values => {
        var server_list = [];
    
        values.forEach( arr => {
            arr.forEach(element => {
                server_list.push(element);
            });
        });

        server_list.sort(function (a, b) {
            if (a.players.current > b.players.current) { 
                return -1;
            } else if(a.players.current < b.players.current) {
                return 1;
            } else {
                return 0;
            }
        });
        log("refreshServerList(): Updating Cache");
        var server_list_cache = {last_updated_ts:  getTimestampMs(), server_list: server_list};
        setServerlistCache(server_list_cache);
        setTimeout(refreshServerLists, CONFIG.serverListRefreshIntervalSeconds * 1000);
    });
}

function onError(error) {
    console.log(`Error: ${error}`);
}
  
function onGot(item) {
    log("Configuration updated:");
    if (item.max_pages) {
      CONFIG.max_pages = item.max_pages;
    }
    if (item.serverListRefreshIntervalSeconds) {
        CONFIG.serverListRefreshIntervalSeconds = item.serverListRefreshIntervalSeconds;
    }
    if (item.playButtonOverride != null) {
        CONFIG.playButtonOverride = item.playButtonOverride;
    }
    log("CONFIG: " + JSON.stringify(CONFIG));
}

function tabMessageReceived(message) {
    if (message.config_update) {
        onGot(message.config_update);
    }
}

function getFullestAvailableServer(server_list) {
   if (server_list.length == 0) {
       return null;
   }
   log("Joining server with player count: " + server_list[0].players.current);
   return server_list[0].guid;
}

function getEmptiestServer(server_list) {
    if (server_list.length == 0) {
        return null;
    }
    log("Joining server with player count: " + server_list[server_list.length-1].players.current);
    return server_list[server_list.length-1].guid;
}

function getServerGuidToJoin() {
    var server_guid = null;
    if (CONFIG.playButtonOverride == "last_server") {
        server_guid = getLastServerPlayed();
    }

    if (!server_guid) {
        var list_cache = getServerListCache();
        if (!list_cache || !list_cache.server_list || list_cache.server_list.length == 0) {
            alert("There are no servers cached yet. Check the console");
            return;
        }
        if(CONFIG.playButtonOverride != "most_empty") {
            server_guid = getFullestAvailableServer(list_cache.server_list);
        }
        if (!server_guid) {
            server_guid = getEmptiestServer(list_cache.server_list);
        }
    }
    return server_guid;
}

function executePageSideJoinServer() {
    var server_guid = getServerGuidToJoin();
    let newScript = document.createElement('script');
    log("Executing 'Roblox.GameLauncher.joinGameInstance(" + CONFIG.place_id +  ", " + server_guid + ");'");
    newScript.innerHTML= 'Roblox.GameLauncher.joinGameInstance("' + CONFIG.place_id + '", "' + server_guid +'");';
    log("Adding script: " + newScript.innerHTML);
    document.head.appendChild(newScript);
    newScript.remove();
}

log("initializing game page");
var CONFIG = {
    place_id: getPlaceId(window.location.pathname),
    max_pages: 1,
    serverListRefreshIntervalSeconds: 10,
    playButtonOverride: "last_server"
}

var ORIGINAL_PLAY_BUTTON =  document.querySelector("#MultiplayerVisitButton");
ORIGINAL_PLAY_BUTTON.hidden=true;
var OVERRIDE_PLAY_BUTTON =  document.querySelector("#OVERRIDE_PLAY_BUTTON");
if (!OVERRIDE_PLAY_BUTTON) {
    OVERRIDE_PLAY_BUTTON = ORIGINAL_PLAY_BUTTON.cloneNode(true);
    ORIGINAL_PLAY_BUTTON.parentNode.insertBefore(OVERRIDE_PLAY_BUTTON, ORIGINAL_PLAY_BUTTON.nextSibling);
}
OVERRIDE_PLAY_BUTTON.hidden=false;
OVERRIDE_PLAY_BUTTON.id = "OVERRIDE_PLAY_BUTTON";
OVERRIDE_PLAY_BUTTON.onclick = function() {
    executePageSideJoinServer();
}


document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.rbx-game-server-join')) return;

    var server_guid = event.target.parentNode.parentNode.getAttribute("data-gameid");
    
    setLastServerPlayed(server_guid);
	
}, false);


browser.runtime.onMessage.addListener(tabMessageReceived);
browser.storage.sync.get("max_pages").then(onGot, onError);
browser.storage.sync.get("serverListRefreshIntervalSeconds").then(onGot, onError);
browser.storage.sync.get("playButtonOverride").then(onGot, onError);


//Disabling refresh lists for now.
//begin interval
refreshServerLists();   
