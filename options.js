
function saveOptions(e) {
    console.log("saveOptions()");
    e.preventDefault();
    var message = {
        config_update: {
            max_pages: document.querySelector("#max_pages").value,
            serverListRefreshIntervalSeconds: document.querySelector("#serverListRefreshIntervalSeconds").value,
            playButtonOverride: document.querySelector("#playButtonOverride").value
        }
    };
    
    browser.storage.sync.set(message);
    sendMessages(message);
}

function restoreOptions() {
    function setMaxPages(result) {
        document.querySelector("#max_pages").value = result.max_pages || "5";
    }
    function setServerListRefreshIntervalSeconds(result) {
        document.querySelector("#serverListRefreshIntervalSeconds").value = result.serverListRefreshIntervalSeconds || "29";
    }
    function setPlayButtonOverride(result) {
        document.querySelector("#playButtonOverride").value  = result.playButtonOverride || "last_server";
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }



    browser.storage.sync.get("max_pages").then(setMaxPages, onError);
    browser.storage.sync.get("serverListRefreshIntervalSeconds").then(setServerListRefreshIntervalSeconds, onError);
    browser.storage.sync.get("playButtonOverride").then(setPlayButtonOverride, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);

function sendMessages(message) {
    browser.tabs.query( { url: "https://www.roblox.com/games/*"}).then( tabs => {
      for (let tab of tabs) {
        browser.tabs.sendMessage(tab.id, message);
      }
    });
  }
  