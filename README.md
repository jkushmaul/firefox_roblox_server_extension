## Roblox Server Addon


![](https://gitlab.com/jkushmaul/firefox_roblox_server_extension/raw/master/icon.png "Roblox server utility")

* Roblox server UI is enough most of the time.  But some problems.
  * Sometimes you want to search for a server with just 1 or 2 open slots.

* You can't rejoin servers.
  * You can get kicked for doing too good
  * Now you can rejoin the same server easily

* They show you full servers when you can't join them.
  * Most games allow you to filter that out

* Sometimes to grind some resources you want a server with few players.  Good luck, infinite scroll.
  * Now you can prefetch a page count, and filter them.

* When your friend is playing, you have to visit their profile and join them.
  * Now you can just click play and it will auto-join you to their server. (May have been addressed since written)

* There is no other mechanism for servers. (besides extensions like this)
  * Now you can search/sort by Ping, FPS, Guid, capcity, (players not possible (within reason) for good reasons)


## Configuration
1. MaxRowsToFetch - Default to 10 to keep roblox off the trail by default.  If they set it to 100, it will make 100 requests.
2. Cache TTL in minutes - When rows are cached, the entire set will be refreshed after this TTL expires from creation.
3. Show "Play last server"
4. Show "Play empty server"
5. Show "Play fullest server"
6. Servers tab sort order: [Ping, Fps, Available Capacity]
7. Servers tab hide full
8. Servers tab hide empty

## Implementation
1. When https://www.roblox.com/games/${path.placeId}/* is loaded
   content_script needs to grab the URL and extract the ${path.placeId} parameter.  This is the storage key prefix for this tab.
   my hope is that this is only visible to the current tab.
2. If cached storage is not present, or, the TTL of cache TS has expired  
   Fetch `${maxRows}` of servers using:  
   `https://www.roblox.com/games/getgameinstancesjson?placeId=${path.placeId}&startIndex=${0..$maxRows*10}&_=${timestamp.ms}`  
   2a. Using json path above, create new json and store in cache with current TS and prefix (placeId).
   2b. This will be performed while the tab is active.
3. Add these play button override options:  
   You can't use the existing play button logic because there's no obvious (and beyond) way to determine what server you were sent to.
    * "Play last server joined" - If no server exists, it picks fullest server.
    * "Play least populated server"
    * "Play most populated server"  
4. If servers tab is clicked  
  4a. Create a new sortable table which sorts the cache.  
  4b. Add Join button which tracks last server joined.


When clicking the |> play button, there is no obvious (and beyond) way to determine from the extension level,
what server you are joining. 


When clicking on servers tab, 3 URLS are fetched but this one is the list of servers:
* `https://www.roblox.com/games/getgameinstancesjson?placeId=${path.placeId}&startIndex=${row}&_=${timestamp.ms}`  
With the following parameters:  
* `${timestamp.ms}` = ms since epoch.
* `${place.id}` = the place id from the game URL.
* `${row}` = the 0 index row to start the page, 10 at a time
I've read of possibly another api endpoint - that will perhaps allow for finer tuned searching...

That returns json of this format:
```
{
    "PlaceId":292439477,
    "ShowShutdownAllButton":false,
    "Collection": [
        {
            "Capacity":32,
            "Ping":101,
            "Fps":59.973099,
            "ShowSlowGameMessage":false,
            "Guid":"159b0ec1-d10d-4029-8372-29ed7603b3a9",
            "PlaceId":292439477,
            "CurrentPlayers":[
                {
                    "Id":0,
                    "Username":"A Roblox Player",
                    "Thumbnail": {
                        "AssetId":0,
                        "AssetHash":null,
                        "AssetTypeId":0,
                        "Url":"https://tr.rbxcdn.com/3db0fb83d06beb4XYZ0d71c1fe7a/48/48/AvatarHeadshot/Png",
                        "IsFinal":true
                        }
                },
                ...
            ]
        },
        ...
    ]
}
```
Each server is the following json path
* `$.Collection[*].Guid`
* `$.Collection[*].Ping`
* `$.Collection[*].Fps`
* `$.Collection[*].Capacity`
* `$.Collection[*].CurrentPlayers.length`


When you click "Join" on a server (Not the main play button - that's different, explained later)  
This Javascript is invoked:  
 ` Roblox.GameLauncher.joinGameInstance(${path.placeId}, "${server.guid}")`

## Configuration
1. MaxRowsToFetch - Default to 10 to keep roblox off the trail by default.  If they set it to 100, it will make 100 requests.
2. Cache TTL in minutes - When rows are cached, the entire set will be refreshed after this TTL expires from creation.
3. Show "Play last server"
4. Show "Play empty server"
5. Show "Play fullest server"
6. Servers tab sort order: [Ping, Fps, Available Capacity]
7. Servers tab hide full
8. Servers tab hide empty

## Implementation
1. When https://www.roblox.com/games/${path.placeId}/* is loaded
   content_script needs to grab the URL and extract the ${path.placeId} parameter.  This is the storage key prefix for this tab.
   my hope is that this is only visible to the current tab.
2. If cached storage is not present, or, the TTL of cache TS has expired  
   Fetch `${maxRows}` of servers using:  
   `https://www.roblox.com/games/getgameinstancesjson?placeId=${path.placeId}&startIndex=${0..$maxRows*10}&_=${timestamp.ms}`  
   2a. Using json path above, create new json and store in cache with current TS and prefix (placeId).
   2b. This will be performed while the tab is active.
3. Add these play button override options:  
   You can't use the existing play button logic because there's no obvious (and beyond) way to determine what server you were sent to.
    * "Play last server joined" - If no server exists, it picks fullest server.
    * "Play least populated server"
    * "Play most populated server"  
4. If servers tab is clicked  
  4a. Create a new sortable table which sorts the cache.  
  4b. Add Join button which tracks last server joined.